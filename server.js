var express = require('express'),
  multer = require('multer'),
  fs = require('fs'),
  path = require('path');

var destFolder = 'images';

var upload = multer({dest: destFolder + '/'});

var app = express();
app.use(express.static(destFolder));
app.post('/', upload.single('image'), function (req, res) {
  var extension = path.extname(req.file.originalname);
  fs.rename(req.file.path, req.file.path + extension);
  res.send(req.file.filename + extension);
});

app.listen(3001);
